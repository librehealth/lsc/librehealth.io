+++
title = "Demo Servers"
+++
## Demo servers
The demo servers are meant to showcase the latest developments of the LibreHealth project. Each project should add a link to their demo site through this page.

**We restore from a clean database every night at 00:00 UTC.**

## LibreHealth Toolkit LTS 2.0.0 demo
LibreHealth Toolkit LTS 2.0.0 demo is a deployment that is done by GitLab CI after every commit.

 - URL - https://toolkit.librehealth.io/master
 - Username: Admin
 - Password: Admin123

## LibreHealth Radiology
LibreHealth Radiology is a customized version of LibreHealth Toolkit with additional tools for radiology and imaging professionals.

 - URL - https://radiology.librehealth.io
 - Username: Admin
 - Password: Admin123

## LibreHealth EHR demo

**Note**: The LibreHealth EHR Demo Servers were taken offline due to security vulnerabilies in the code. 

This contains no real patient data.

## Bleeding Edge (tracking `master` branch)
 - URL - https://ehr.librehealth.io/develop
 - Username: admin
 - Pass Phrase: password

## LibreHealth EHR w/ The National Health and Nutrition Examination Survey (NHANES) data

An instance of LibreHealth EHR has been deployed with data from NHANES. You can read more about it [on our forum](https://forums.librehealth.io/t/nhanes-data-in-libreehr/735).

You can also download the data [here](https://nhanes.librehealth.io/downloads/) for local use.

- URL: https://nhanes.librehealth.io
- Username: admin
- Pass Phrase: password
